<?php

namespace PlanetaDelEste\BuddiesGroup\Classes\Store\User;

use Lovata\Buddies\Models\User;
use Lovata\Toolbox\Classes\Store\AbstractStoreWithParam;
use October\Rain\Database\Builder;

class ListByGroupStore extends AbstractStoreWithParam
{
    protected static $instance;

    /**
     * @inheritDoc
     */
    protected function getIDListFromDB(): array
    {
        return User::whereHas(
            'groups',
            function (Builder $obQuery) {
                $arValue = array_map('trim', explode('|', $this->sValue));
                $sColumn = is_numeric($arValue[0]) ? 'id' : 'code';

                return $obQuery->whereIn($sColumn, $arValue);
            }
        )->lists('id');
    }
}
